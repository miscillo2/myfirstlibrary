var _app = angular.module('my.first.library', []);

_app.directive('welcomeMessage', function(){
    return {
        restrict: 'E',
        templateUrl: '/directives/welcome.html'
      };
});

/*
//Configurazione Necessaria per poter provare singolarmente questo mini-progetto
app.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
        // HOME STATES AND NESTED VIEWS ========================================
        .state('home', {
            url: '/home',
            templateUrl: 'partials/home.html'
        })
    $urlRouterProvider.otherwise('/home');
});

app.controller('MainController', function($scope){
   $scope.msg = "Ciao, sono il controller!";
});
*/